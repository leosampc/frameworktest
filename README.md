# FrameworkTest

## Description

An app that lists Posts, Albums and Todos lists from the `https://jsonplaceholder.typicode.com/` API.

> Based on Google Material Design 2 and 3 versions.

## Getting started

`git clone https://gitlab.com/leosampc/frameworktest.git`

`yarn` || `npm install`

- Android:

* `yarn clean` || `npm run clean`
* `yarn android` || `npm run android`

- iOS:

* `yarn pods` || `npm run pods`
* `yarn ios` || `npm run ios`

## Used Concepts

- Offline First
- Design Patterns
- Clean Code
- SOLID

import React from 'react';
import { useSelector } from 'react-redux';
import { useAlbums } from '@/hooks';
import { List, Screen } from '@/components';

function AlbumsList() {
  const { data, isLoading } = useSelector((state) => state.albums);
  const { retrieveAlbums } = useAlbums();
  return (
    <Screen>
      <List
        data={data}
        emptyMessage="Albums is empty."
        onRefresh={() => retrieveAlbums()}
        refreshing={isLoading}
      />
    </Screen>
  );
}

export default AlbumsList;

import React from 'react';
import { useSelector } from 'react-redux';
import { usePosts } from '@/hooks';
import { List, Screen } from '@/components';

function PostsList() {
  const { data, isLoading } = useSelector((state) => state.posts);
  const { retrievePosts } = usePosts();
  return (
    <Screen>
      <List
        data={data}
        emptyMessage="Posts is empty."
        onRefresh={() => retrievePosts()}
        refreshing={isLoading}
      />
    </Screen>
  );
}

export default PostsList;

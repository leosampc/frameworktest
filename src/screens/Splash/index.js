import React, { useEffect, useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import { usePosts, useAlbums, useTodos, useNetInfo, usePrevious } from '@/hooks';
import { Text } from '@/components';
import { StyledScreen } from './styles';

function Splash() {
  const navigation = useNavigation();
  const { retrievePosts } = usePosts();
  const { retrieveAlbums } = useAlbums();
  const { retrieveTodos } = useTodos();
  const { isOffline } = useNetInfo();
  const prevIsOffline = usePrevious(isOffline);

  const syncData = useCallback(async () => {
    await retrievePosts();
    await retrieveAlbums();
    await retrieveTodos();
  }, [retrievePosts, retrieveAlbums, retrieveTodos]);

  useEffect(() => {
    async function initialize() {
      await syncData();
      navigation.push('Home');
    }

    initialize();
  }, [navigation, syncData]);

  useEffect(() => {
    if (prevIsOffline !== undefined && !isOffline) {
      syncData();
    }
  }, [isOffline, prevIsOffline, syncData]);
  return (
    <StyledScreen headerOff backgroundColor="primaryDark">
      <Text color="white" fontSize={22} fontWeight={500}>
        FrameworkTest
      </Text>
    </StyledScreen>
  );
}

export default Splash;

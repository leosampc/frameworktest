import styled from 'styled-components/native';
import { Screen } from '@/components';

export const StyledScreen = styled(Screen)`
  justify-content: center;
  align-items: center;
`;

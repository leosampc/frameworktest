import React from 'react';
import { useSelector } from 'react-redux';
import { useTodos } from '@/hooks';
import { List, Screen } from '@/components';

function TodosList() {
  const { data, isLoading } = useSelector((state) => state.todos);
  const { retrieveTodos } = useTodos();
  return (
    <Screen>
      <List
        data={data}
        emptyMessage="Todos is empty."
        onRefresh={() => retrieveTodos()}
        refreshing={isLoading}
      />
    </Screen>
  );
}

export default TodosList;

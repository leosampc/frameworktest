import Realm from 'realm';

import { Posts, Albums, Todos } from './schemas';

const realm = new Realm({ schema: [Posts, Albums, Todos] });

function findAll(schema) {
  return realm.objects(schema).map((object) => object) || [];
}

function write(data, schema) {
  data.forEach((content) => {
    realm.write(() => {
      realm.create(schema, content, 'modified');
    });
  });
}

export { findAll, write };

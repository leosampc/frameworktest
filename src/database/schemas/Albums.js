import Realm from 'realm';

class Albums extends Realm.Object {}
Albums.schema = {
  name: 'Albums',
  properties: {
    id: 'int',
    userId: 'int',
    title: 'string',
  },
  primaryKey: 'id',
};

export default Albums;

import Posts from './Posts';
import Albums from './Albums';
import Todos from './Todos';

export { Posts, Albums, Todos };

import Realm from 'realm';

class Todos extends Realm.Object {}
Todos.schema = {
  name: 'Todos',
  properties: {
    id: 'int',
    userId: 'int',
    title: 'string',
    completed: { type: 'bool', default: false },
  },
  primaryKey: 'id',
};

export default Todos;

import Realm from 'realm';

class Posts extends Realm.Object {}
Posts.schema = {
  name: 'Posts',
  properties: {
    id: 'int',
    userId: 'int',
    title: 'string',
    body: 'string',
  },
  primaryKey: 'id',
};

export default Posts;

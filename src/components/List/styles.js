import styled from 'styled-components/native';

const StyledFlatList = styled.FlatList.attrs(() => ({
  contentContainerStyle: {
    flexGrow: 1,
  },
}))``;

export default StyledFlatList;

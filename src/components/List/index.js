import React from 'react';
import PropTypes from 'prop-types';
import StyledFlatList from './styles';

import ListItem from './ListItem';
import ListEmptyComponent from './ListEmptyComponent';
import ItemSeparatorComponent from './ItemSeparatorComponent';

function List({ data, emptyMessage, onRefresh, refreshing }) {
  return (
    <StyledFlatList
      removeClippedSubviews
      maxToRenderPerBatch={12}
      initialNumToRender={10}
      keyExtractor={(item) => `${item.id}`}
      data={data}
      extraData={data}
      renderItem={({ item }) => <ListItem item={item} />}
      ItemSeparatorComponent={ItemSeparatorComponent}
      onRefresh={onRefresh}
      refreshing={refreshing}
      ListEmptyComponent={<ListEmptyComponent message={emptyMessage} isLoading={refreshing} />}
    />
  );
}

List.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      userId: PropTypes.number,
      id: PropTypes.number,
      title: PropTypes.string,
      body: PropTypes.string,
      completed: PropTypes.bool,
    })
  ),
  emptyMessage: PropTypes.string,
  onRefresh: PropTypes.func,
  refreshing: PropTypes.bool,
};

List.defaultProps = {
  data: undefined,
  emptyMessage: 'List is empty.',
  onRefresh: undefined,
  refreshing: false,
};

export default List;

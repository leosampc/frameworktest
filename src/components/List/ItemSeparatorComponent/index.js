import React from 'react';
import styled from 'styled-components/native';

const ItemSeparatorComponent = styled.View.attrs((props) => ({
  style: {
    ...props.theme.shadow,
  },
}))`
  height: ${(props) => props.theme.normalize(0.5)}px;
  background-color: ${(props) => `${props.theme.colors.secondaryLightColor}4D`};
  elevation: 2;
`;

export default React.memo(ItemSeparatorComponent);

import React from 'react';
import PropTypes from 'prop-types';
import Text from '@/components/Text';
import Container from './styles';

function ListEmptyComponent({ message, isLoading }) {
  return (
    <Container>
      <Text color="secondaryLight">{isLoading ? 'Fetching...' : message}</Text>
    </Container>
  );
}

ListEmptyComponent.propTypes = {
  isLoading: PropTypes.bool,
  message: PropTypes.string,
};

ListEmptyComponent.defaultProps = {
  isLoading: false,
  message: 'List is empty.',
};

export default ListEmptyComponent;

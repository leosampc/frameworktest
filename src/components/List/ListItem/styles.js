import styled from 'styled-components/native';

export const StyledListItem = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: ${(props) => props.theme.normalize(70)}px;
  padding-horizontal: ${(props) => props.theme.normalize(20)}px;
`;

export const TextsWrapper = styled.View`
  justify-content: space-around;
  width: 90%;
`;

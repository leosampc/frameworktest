import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Helpers } from '@/utils';
import Text from '@/components/Text';
import Icon from '@/components/Icon';
import theme from '@/theme';
import { StyledListItem, TextsWrapper } from './styles';

function ListItem({ item }) {
  function renderCompleted() {
    if (item.completed === undefined) {
      return null;
    }
    return item.completed ? (
      <Icon iconSet="ionicons" name="checkmark-done-sharp" color="primaryDark" size={20} />
    ) : (
      <Icon
        iconSet="ionicons"
        name="checkmark-sharp"
        color={`${theme.colors.secondaryLightColor}99`}
        size={20}
      />
    );
  }
  return (
    <StyledListItem>
      <TextsWrapper>
        <Text fontSize={16} fontWeight={400} numberOfLines={item.body ? 1 : 2}>
          {Helpers.captalizeFisrtLetterOf(item.title)}
        </Text>
        {item.body && (
          <Text fontSize={12} fontWeight="normal" numberOfLines={1} color="secondaryLight">
            {Helpers.captalizeFisrtLetterOf(item.body)}
          </Text>
        )}
      </TextsWrapper>
      {renderCompleted()}
    </StyledListItem>
  );
}

ListItem.propTypes = {
  item: PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
    completed: PropTypes.bool,
  }).isRequired,
};

export default memo(ListItem);

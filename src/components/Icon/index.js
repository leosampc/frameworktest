import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, ViewStyle } from 'react-native';
import IconComponent from './IconComponent';

function Icon({ isTouchable, size, color, iconSet, onPress, ...props }) {
  if (isTouchable) {
    return (
      <TouchableOpacity onPress={onPress} style={props.style}>
        <IconComponent size={size} color={color} iconSet={iconSet} {...props} />
      </TouchableOpacity>
    );
  }
  return <IconComponent size={size} color={color} iconSet={iconSet} {...props} />;
}

Icon.propTypes = {
  isTouchable: PropTypes.bool,
  size: PropTypes.number,
  color: PropTypes.string,
  iconSet: PropTypes.oneOf([
    'antd',
    'material',
    'materialCommunity',
    'feather',
    'fa',
    'fontisto',
    'entypo',
    'simpleLine',
    'ionicons',
    'octicons',
  ]),
  onPress: PropTypes.func,
  style: PropTypes.shape(ViewStyle),
};

Icon.defaultProps = {
  isTouchable: false,
  size: 20,
  color: 'primary',
  iconSet: 'material',
  onPress: () => null,
  style: null,
};

export default Icon;

import React from 'react';
import PropTypes from 'prop-types';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FeatherIcons from 'react-native-vector-icons/Feather';
import Octicons from 'react-native-vector-icons/Octicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';

function IconSet({ iconSet, ...props }) {
  if (iconSet === 'material') {
    return <MaterialIcons {...props} />;
  }
  if (iconSet === 'materialCommunity') {
    return <MaterialCommunityIcons {...props} />;
  }
  if (iconSet === 'feather') {
    return <FeatherIcons {...props} />;
  }
  if (iconSet === 'fa') {
    return <FontAwesome {...props} />;
  }
  if (iconSet === 'fa5') {
    return <FontAwesome5 {...props} />;
  }
  if (iconSet === 'fontisto') {
    return <Fontisto {...props} />;
  }
  if (iconSet === 'entypo') {
    return <Entypo {...props} />;
  }
  if (iconSet === 'simpleLine') {
    return <SimpleLineIcons {...props} />;
  }
  if (iconSet === 'ionicons') {
    return <Ionicons {...props} />;
  }
  if (iconSet === 'octicons') {
    return <Octicons {...props} />;
  }
  return <AntDesign {...props} />;
}

IconSet.propTypes = {
  iconSet: PropTypes.oneOf([
    'antd',
    'material',
    'materialCommunity',
    'feather',
    'fa',
    'fontisto',
    'entypo',
    'simpleLine',
    'ionicons',
    'octicons',
  ]),
};

IconSet.defaultProps = {
  iconSet: 'antd',
};

export default IconSet;

import React from 'react';
import PropTypes from 'prop-types';
import theme from '@/theme';
import IconSet from './IconSet';

function IconComponent({ ...props }) {
  const size = theme.normalize(props.size);
  const color = theme.colors[`${props.color}Color`] || props.color;
  return <IconSet style={{ textShadowRadius: 0 }} {...props} size={size} color={color} />;
}

IconComponent.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

IconComponent.defaultProps = {
  size: 20,
  color: 'paper',
};

export default IconComponent;

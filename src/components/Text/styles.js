import styled from 'styled-components';

const StyledText = styled.Text`
  font-size: ${(props) => props.theme.normalize(props.fontSize)}px;
  ${(props) => props.fontWeight && `font-weight: ${props.fontWeight};`}
  color: ${({ color, ...props }) => {
    if (color) {
      return props.theme.colors[`${color}Color`] || color;
    }
    return props.theme.colors.paperColor;
  }};
  text-align: ${({ align }) => align};
  ${({ width, ...props }) => !!width && `width: ${props.theme.normalize(width)}px;`}
`;

export default StyledText;

import React from 'react';
import PropTypes from 'prop-types';
import StyledText from './styles';

function Text({ children, fontWeight, fontSize, numberOfLines, color, width, align, ...props }) {
  return (
    <StyledText
      fontSize={fontSize}
      color={color}
      fontWeight={fontWeight}
      width={width}
      align={align}
      numberOfLines={numberOfLines}
      {...props}
    >
      {children}
    </StyledText>
  );
}

Text.propTypes = {
  children: PropTypes.node,
  fontWeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  fontSize: PropTypes.number,
  numberOfLines: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.number,
  align: PropTypes.oneOf(['auto', 'left', 'right', 'center', 'justify']),
};

Text.defaultProps = {
  children: null,
  fontWeight: 'normal',
  fontSize: 14,
  numberOfLines: 0,
  color: 'black',
  width: null,
  align: 'left',
};

export default Text;

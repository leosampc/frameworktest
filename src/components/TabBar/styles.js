import styled from 'styled-components';
import { TouchableOpacity } from 'react-native';
import { Device } from '@/utils';

export const TabBarContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding-vertical: ${(props) => props.theme.normalize(10)}px;
  background-color: ${(props) => props.theme.colors.primaryLightColor};
  ${(props) => Device.hasNotch && `padding-bottom: ${props.theme.normalize(22)}px;`}
  elevation: 20;
`;

export const IconButton = styled(TouchableOpacity)`
  align-items: center;
  justify-content: flex-end;
  margin-horizontal: ${(props) => props.theme.normalize(20)}px;
  width: ${(props) => props.theme.normalize(75)}px;
`;

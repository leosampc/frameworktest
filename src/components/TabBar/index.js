/* eslint-disable react/prop-types */
/* eslint-disable no-nested-ternary */
import React from 'react';
import theme from '@/theme';
import Text from '../Text';
import Icon from '../Icon';
import { TabBarContainer, IconButton } from './styles';

const MapRoutesToIcons = {
  Posts: {
    name: 'web',
    iconSet: 'material',
  },
  Albums: {
    name: 'albums-outline',
    iconSet: 'ionicons',
  },
  Todos: {
    name: 'list',
    iconSet: 'entypo',
  },
};

export default function TabBar({ state, descriptors, navigation }) {
  function getLabel(options, route) {
    if (options.tabBarLabel !== undefined || options.title !== undefined) {
      return options.tabBarLabel !== undefined ? options.tabBarLabel : options.title;
    }
    return route.name;
  }

  return (
    <TabBarContainer style={theme.shadow}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label = getLabel(options, route);

        const isFocused = state.index === index;
        const color = isFocused ? 'secondaryDark' : 'secondaryLight';

        const { name, iconSet } = MapRoutesToIcons[route.name];

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <IconButton
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            key={route.name}
          >
            <Icon name={name} iconSet={iconSet} size={19} color={color} />
            <Text style={{ marginTop: 3 }} fontSize={12} fontWeight={500} color={color}>
              {label}
            </Text>
          </IconButton>
        );
      })}
    </TabBarContainer>
  );
}

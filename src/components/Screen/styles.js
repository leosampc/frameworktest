import styled from 'styled-components/native';

export const ScreenContainer = styled.View`
  flex: 1;
  background-color: ${({ theme, backgroundColor }) =>
    theme.colors[`${backgroundColor}Color`] || backgroundColor || theme.colors.whiteColor};
`;

export const Body = styled.View`
  flex: 1;
`;

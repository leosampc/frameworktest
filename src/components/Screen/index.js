import React from 'react';
import { StatusBar, Platform } from 'react-native';
import { useRoute } from '@react-navigation/native';
import theme from '@/theme';
import PropTypes from 'prop-types';
import Header from '../Header';
import { ScreenContainer, Body } from './styles';

function Screen({ children, headerOff, backgroundColor, ...props }) {
  const route = useRoute();
  return (
    <ScreenContainer backgroundColor={backgroundColor}>
      <StatusBar
        backgroundColor={
          Platform.OS === 'android' &&
          (theme.colors[`${backgroundColor}Color`] ||
            backgroundColor ||
            theme.colors.primaryDarkColor)
        }
        barStyle="light-content"
      />
      {!headerOff && <Header title={route.name} />}
      <Body {...props}>{children}</Body>
    </ScreenContainer>
  );
}

Screen.propTypes = {
  children: PropTypes.node,
  headerOff: PropTypes.bool,
  backgroundColor: PropTypes.string,
};

Screen.defaultProps = {
  children: null,
  headerOff: false,
  backgroundColor: undefined,
};

export default Screen;

import styled from 'styled-components/native';
import { Device } from '@/utils';
import Text from '../Text';

export const HeaderContainer = styled.View`
  justify-content: ${Device.hasNotch ? 'flex-end' : 'center'};
  height: ${(props) => props.theme.normalize(60 + (Device.hasNotch ? 30 : 0))}px;
  padding-horizontal: ${(props) => props.theme.normalize(25)}px;
  padding-vertical: ${(props) => props.theme.normalize(10)}px;
  background-color: ${(props) => props.theme.colors.primaryDarkColor};
  elevation: 20;
`;

export const HeaderLeftWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Title = styled(Text).attrs((props) => ({
  fontWeight: 500,
  fontSize: 28,
  color: props.theme.colors.whiteColor,
}))``;

import React from 'react';
import PropTypes from 'prop-types';
import theme from '@/theme';
import { HeaderContainer, HeaderLeftWrapper, Title } from './styles';

function Header({ title }) {
  return (
    <HeaderContainer testID="headerContainer" style={theme.shadow}>
      <HeaderLeftWrapper>
        <Title>{title}</Title>
      </HeaderLeftWrapper>
    </HeaderContainer>
  );
}

Header.propTypes = {
  title: PropTypes.string,
};

Header.defaultProps = {
  title: '',
};

export default Header;

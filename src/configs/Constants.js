import { Platform } from 'react-native';

/* 
  I had to use this approach because for some reason I have a problem
  with the SSL certificate of "https://jsonplaceholder.typicode.com",
  so api requests not working on android and were replaying
  a messages like: "axios Chain validation failed".
  After researching on the subject, I found some links
  for the possible solution, but they seem a bit "laborious" to
  do in such a simple project, so I prefer to do it this way because
  it's simpler and faster.

  Here has an examples of the links that I mentioned before:
  * https://github.com/axios/axios/issues/535#:~:text=Do%20you%20know%20how%20to%20ignore%20the%20ssl%20issue%20in%20React%20Native%3F
  * https://stackoverflow.com/questions/40240321/how-can-i-implement-ssl-certificate-pinning-while-using-react-native/40334166#40334166
*/

const baseURL = 'jsonplaceholder.typicode.com';

function retrieveBaseURL() {
  let httpString = 'https';
  if (Platform.OS === 'android') {
    httpString = 'http';
  }
  return `${httpString}://${baseURL}`;
}

export default { retrieveBaseURL };

import React from 'react';
import { Provider } from 'react-redux';
import store from '@/redux';
import ThemeProvider from './providers';
import AppNavigation from './navigation';

export default function App() {
  return (
    <Provider store={store}>
      <ThemeProvider>
        <AppNavigation />
      </ThemeProvider>
    </Provider>
  );
}

import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TabBar } from '@/components';

import PostsListScreen from '@/screens/PostsList';
import AlbumsListScreen from '@/screens/AlbumsList';
import TodosListScreen from '@/screens/TodosList';

const Tab = createBottomTabNavigator();

function customTabBar(props) {
  return <TabBar {...props} />;
}

function TabNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Posts"
      tabBar={customTabBar}
    >
      <Tab.Screen name="Posts" options={{ title: 'Posts' }} component={PostsListScreen} />
      <Tab.Screen name="Albums" component={AlbumsListScreen} />
      <Tab.Screen name="Todos" component={TodosListScreen} />
    </Tab.Navigator>
  );
}

export default TabNavigator;

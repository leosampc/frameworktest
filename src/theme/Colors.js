const primaryColor = '#64b5f6';
const primaryLightColor = '#9be7ff';
const primaryDarkColor = '#2286c3';
const secondaryColor = '#424242';
const secondaryLightColor = '#6d6d6d';
const secondaryDarkColor = '#1b1b1b';
const blackColor = '#000000';
const whiteColor = '#ffffff';
const textPrimaryColor = blackColor;
const textSecondaryColor = whiteColor;

export default {
  primaryColor,
  primaryLightColor,
  primaryDarkColor,
  secondaryColor,
  secondaryLightColor,
  secondaryDarkColor,
  textPrimaryColor,
  textSecondaryColor,
  blackColor,
  whiteColor,
};

import { Dimensions, PixelRatio, Platform } from 'react-native';

const { width } = Dimensions.get('window');

const getNormalize = (size) => {
  const scale = width / 360;

  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export default {
  shadow: Platform.OS === 'ios' && {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.15,
    shadowRadius: 10.84,
  },
  normalize: getNormalize,
};

import axios from 'axios';
import Constants from '@/configs/Constants';

const api = axios.create({
  baseURL: Constants.retrieveBaseURL(),
  timeout: 3000,
});

export default api;

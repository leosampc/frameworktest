import api from './api';

function fetchAll() {
  return api.get('/albums');
}

export { fetchAll };

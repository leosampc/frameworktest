import api from './api';

function fetchAll() {
  return api.get('/posts');
}

export { fetchAll };

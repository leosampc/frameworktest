import api from './api';

function fetchAll() {
  return api.get('/todos');
}

export { fetchAll };

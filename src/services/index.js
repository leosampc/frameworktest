import * as postsServices from './postsServices';
import * as todosServices from './todosServices';
import * as albumsServices from './albumsServices';

export { postsServices, todosServices, albumsServices };

import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import Database from '@/database';
import { postsServices } from '@/services';

export default () => {
  const dispatch = useDispatch();

  const retrievePosts = useCallback(async () => {
    const updateState = (payload) => dispatch({ type: 'posts/requestSuccess', payload });
    dispatch({ type: 'posts/requestPending' });
    try {
      const { data } = await postsServices.fetchAll();
      Database.write(data, 'Posts');
      updateState(data);
    } catch (error) {
      const data = Database.findAll('Posts');
      updateState(data);
    }
  }, [dispatch]);

  return { retrievePosts };
};

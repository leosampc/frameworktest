import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import Database from '@/database';
import { albumsServices } from '@/services';

export default () => {
  const dispatch = useDispatch();

  const retrieveAlbums = useCallback(async () => {
    const updateState = (payload) => dispatch({ type: 'albums/requestSuccess', payload });
    dispatch({ type: 'albums/requestPending' });
    try {
      const { data } = await albumsServices.fetchAll();
      Database.write(data, 'Albums');
      updateState(data);
    } catch (error) {
      const data = Database.findAll('Albums');
      updateState(data);
    }
  }, [dispatch]);

  return { retrieveAlbums };
};

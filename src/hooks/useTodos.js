import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import Database from '@/database';
import { todosServices } from '@/services';

export default () => {
  const dispatch = useDispatch();

  const retrieveTodos = useCallback(async () => {
    const updateState = (payload) => dispatch({ type: 'todos/requestSuccess', payload });
    dispatch({ type: 'todos/requestPending' });
    try {
      const { data } = await todosServices.fetchAll();
      Database.write(data, 'Todos');
      updateState(data);
    } catch (error) {
      const data = Database.findAll('Todos');
      updateState(data);
    }
  }, [dispatch]);

  return { retrieveTodos };
};

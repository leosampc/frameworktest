import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [],
  loading: false,
};

const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    requestPending: (state) => {
      state.loading = true;
    },
    requestSuccess: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },
  },
});

export const { requestPending, requestSuccess } = todosSlice.actions;

export default todosSlice.reducer;

import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
  data: [],
  loading: false,
};

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    requestPending: (state) => {
      state.loading = true;
    },
    requestSuccess: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },
  },
});

export const { requestPending, requestSuccess } = postsSlice.actions;

export default postsSlice.reducer;

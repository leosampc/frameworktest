import { configureStore } from '@reduxjs/toolkit';
import postsReducer from './posts';
import albumsReducer from './albums';
import todosReducer from './todos';

const store = configureStore({
  reducer: {
    posts: postsReducer,
    albums: albumsReducer,
    todos: todosReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;

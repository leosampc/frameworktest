import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [],
  loading: false,
};

const albumsSlice = createSlice({
  name: 'albums',
  initialState,
  reducers: {
    requestPending: (state) => {
      state.loading = true;
    },
    requestSuccess: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },
  },
});

export const { requestPending, requestSuccess } = albumsSlice.actions;

export default albumsSlice.reducer;
